FROM python:3.10-slim

COPY ./src /app
COPY requirements.txt /requirements.txt
WORKDIR /app

# RUN apk update && apk add python3-dev \
#                           gcc \
#                           libc-dev \
#                           libffi-dev


RUN pip install -r /requirements.txt

CMD ["python", "kucoin_app.py"]
