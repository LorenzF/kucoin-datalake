class Minio:
    ENDPOINT = '<minio_endpoint>'
    KEY = '<minio_key>'
    SECRET = '<minio_secret>'
    BUCKET = '<datalake_bucket>'
    CRYPTO_TABLE = '<coins_table>'
    NEWS_TABLE = '<news_table>'


class KuCoin:
    KEY = '<your_kucoin_key>'
    SECRET = '<your_kucoin_secret>'
    PASSPRHASE = '<your_kucoin_password>'


class CryptoPanic:
    KEY = '<cyptopanic_key'
