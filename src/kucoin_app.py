import asyncio
import datetime
import time
from collections import deque
import logging
import threading
import requests

import pandas as pd
from kucoin.client import WsToken, Market
from kucoin.ws_client import KucoinWsClient
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from fastapi import FastAPI
import pyarrow as pa
from pyarrow import parquet as pq
import s3fs


from config import Minio, KuCoin, CryptoPanic

app = FastAPI()
pairs = {
    'ADA-USDC',
    'ALGO-USDC',
    'DOGE-USDC',
    'ETH-USDC',
    'KCS-USDC',
    'MATIC-USDC',
    'XLM-USDT',
}

ws_client = None
buffer = {}
stage = {}
news = pd.DataFrame(
    [['news', '', 'boom', pd.to_datetime(datetime.datetime(year=2001, month=11, day=9)).tz_localize('UTC')]],
    columns=['type', 'domain', 'title', 'timestamp']
)
last = {}

minio = s3fs.S3FileSystem(
    key=Minio.KEY,
    secret=Minio.SECRET,
    use_ssl=False,
    client_kwargs={
        'endpoint_url': Minio.ENDPOINT,
    }
)

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)
logger = logging.getLogger('kucoin')


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/pairs")
async def get_pairs():
    global pairs
    return pairs


@app.post("/pairs/{pair}")
async def post_pair(pair):
    result = await add_pair(pair)
    return {pair: result}


@app.delete("/pairs/pair")
async def delete_pair(pair):
    result = await remove_pair(pair)
    return {pair: result}


@app.on_event("startup")
async def run_scheduler():
    scheduler = AsyncIOScheduler()
    scheduler.add_job(store_data, 'interval', minutes=1, max_instances=1)
    scheduler.add_job(read_news, 'interval', hours=3, max_instances=1)
    scheduler.start()

    _thread = threading.Thread(target=asyncio.run, args=(start_socket(),))
    _thread.start()


async def deal_msg(msg):
    global last
    global buffer

    symbol = msg['topic'].split(':')[-1]
    try:
        if 'ticker' in msg['topic']: # == '/market/ticker:KCS-USDT':
            if (msg['data']['price'], msg['data']['size']) == last[symbol]:
                return
            else:
                last[symbol] = (msg['data']['price'], msg['data']['size'])
                now = datetime.datetime.now()
                buffer[symbol].append([now,  'price', float(msg['data']['price']), float(msg['data']['size'])])
                buffer[symbol].append([now,  'bestBid', float(msg['data']['bestBid']), float(msg['data']['bestBidSize'])])
                buffer[symbol].append([now,  'bestAsk', float(msg['data']['bestAsk']), float(msg['data']['bestAskSize'])])

        elif 'level2' in msg['topic']:# == '/market/level2:KCS-USDT':
            for ask in msg['data']['changes']['asks']:
                if ask[0] != '0':
                    buffer[symbol].append([datetime.datetime.now(), 'ask', float(ask[0]), float(ask[1])])
            for bid in msg['data']['changes']['bids']:
                if bid[0] != '0':
                    buffer[symbol].append([datetime.datetime.now(), 'bid', float(bid[0]), float(bid[1])])
    except KeyError:
        pass


async def add_pair(pair):
    global ws_client, pairs, buffer, last
    #client = Market()
    client = Market(key=KuCoin.KEY, secret=KuCoin.SECRET, passphrase=KuCoin.PASSPRHASE)

    logger.info(f'starting pair {pair}')
    tickers = [ticker.get('symbol') for ticker in client.get_all_tickers().get('ticker')]
    if pair not in tickers:
        logger.warning(f'pair: {pair} not found!')
        return 'currency pair not found'
    await ws_client.subscribe(f'/market/ticker:{pair}')
    await ws_client.subscribe(f'/market/level2:{pair}')

    pairs.add(pair)
    buffer[pair] = deque()
    stage[pair] = pd.DataFrame()
    last[pair] = ('0', '0')

    book = client.get_aggregated_orderv3(pair)
    if book['time'] == 0:
        logger.info(f'failed to add pair {pair}')
        return 'fail'
    buffer[pair].extend([[datetime.datetime.fromtimestamp(book['time'] / 1000), 'ask', float(x[0]), float(x[1])] for x in book['asks']])
    buffer[pair].extend([[datetime.datetime.fromtimestamp(book['time'] / 1000), 'bid', float(x[0]), float(x[1])] for x in book['bids']])

    return 'success'


async def remove_pair(pair):

    global ws_client, pairs, buffer, last

    if pair not in pairs:
        return 'fail'

    logger.info(f'removing pair {pair}')
    await ws_client.unsubscribe(f'/market/ticker:{pair}')
    await ws_client.unsubscribe(f'/market/level2:{pair}')

    pairs.remove(pair)
    del buffer[pair]
    del last[pair]

    return 'success'


async def start_socket():

    global ws_client, pairs
    token = WsToken()

    ws_client = await KucoinWsClient.create(None, token, deal_msg, private=False)

    for pair in pairs:
        time.sleep(5)
        await add_pair(pair)

    while True:
        await asyncio.sleep(5)


def store_data():
    global buffer, stage
    for sym, buf in buffer.items():
        df = []
        while len(buf) > 0:
            df.append(buf.popleft())

        df = pd.DataFrame(df, columns=['time', 'type', 'price', 'amount'])
        df['pair'] = sym
        df['year'] = df.time.dt.year
        df['month'] = df.time.dt.month
        df['day'] = df.time.dt.day
        df['hour'] = df.time.dt.hour
        df['minute'] = df.time.dt.minute

        stage[sym] = pd.concat(
            [
                stage[sym],
                df
            ]
        )
        last_minute = stage[sym].minute.iloc[-1]
        write_df = stage[sym][stage[sym].minute != last_minute]
        stage[sym] = stage[sym][stage[sym].minute == last_minute]
        write_df = pa.Table.from_pandas(write_df)

        pq.write_to_dataset(
            write_df,
            f's3://{Minio.BUCKET}/{Minio.CRYPTO_TABLE}.parquet',
            filesystem=minio,
            use_dictionary=True,
            compression="snappy",
            version="2.6",
            partition_cols=['pair', 'year', 'month', 'day', 'hour', 'minute']
        )


def read_news():
    global news
    url = f"https://cryptopanic.com/api/v1/posts/?auth_token={CryptoPanic.KEY}"
    response = requests.get(url)

    results = response.json()['results']
    next = response.json()['next']

    latest_news = pd.DataFrame(
        [
            [x['kind'], x['domain'], x['title'], pd.to_datetime(x['published_at'])] for x in results
        ], columns=['type', 'domain', 'title', 'timestamp']
    )
    reading = True
    while (latest_news.timestamp.min() > news.timestamp.max()) & reading:
        time.sleep(0.5)
        response = requests.get(next)
        next = response.json()['next']
        results = response.json()['results']
        if next is None:
            reading = False
        latest_news = pd.concat(
            [
                latest_news,
                pd.DataFrame(
                    [
                        [x['kind'], x['domain'], x['title'], pd.to_datetime(x['published_at'])] for x in results
                    ], columns=['type', 'domain', 'title', 'timestamp']
                )
            ]
        )

    latest_news = latest_news[latest_news.timestamp > news.timestamp.max()]
    news = pd.concat(
        [
            latest_news,
            news
        ]
    )

    today = datetime.date.today()
    dates = set([x.date() for x in news.timestamp]).difference([today])

    for date in dates:
        write_df = news[news.timestamp.dt.date == date].copy()
        write_df['year'] = write_df.timestamp.dt.year
        write_df['month'] = write_df.timestamp.dt.month
        write_df['day'] = write_df.timestamp.dt.day

        pq.write_to_dataset(
            pa.Table.from_pandas(write_df),
            f's3://{Minio.BUCKET}/{Minio.NEWS_TABLE}.parquet',
            filesystem=minio,
            use_dictionary=True,
            compression="snappy",
            version="2.6",
            partition_cols=['year', 'month', 'day']
        )
        news = news[news.timestamp.dt.date != date]



if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8010)
