import asyncio
import os
from datetime import datetime
from uuid import uuid4
import time
from collections import deque
import logging

import pandas as pd
from kucoin.client import WsToken, Market
from kucoin.ws_client import KucoinWsClient

from config import KuCoin

symbols = ['KCS-USDT', 'ETH-USDT', 'SOL-USDT', 'LUNA-USDT']

buffer = {symbol: deque() for symbol in symbols}
last = {symbol: ('0', '0') for symbol in symbols}
parquet_size = 1000
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)


async def deal_msg(msg):
    global last
    global buffer

    symbol = msg['topic'].split(':')[-1]

    try:
        if 'ticker' in  msg['topic']: # == '/market/ticker:KCS-USDT':
            if (msg['data']['price'], msg['data']['size']) == last[symbol]:
                return
            else:
                last[symbol] = (msg['data']['price'], msg['data']['size'])
                now = time.time()
                buffer[symbol].append([int(now),  'price', msg['data']['price'], msg['data']['size']])
                buffer[symbol].append([int(now),  'bestBid', msg['data']['bestBid'], msg['data']['bestBidSize']])
                buffer[symbol].append([int(now),  'bestAsk', msg['data']['bestAsk'], msg['data']['bestAskSize']])

        elif 'level2' in msg['topic']:# == '/market/level2:KCS-USDT':
            for ask in msg['data']['changes']['asks']:
                if ask[0] != '0':
                    buffer[symbol].append([int(time.time()), 'ask', ask[0], ask[1]])
            for bid in msg['data']['changes']['bids']:
                if bid[0] != '0':
                    buffer[symbol].append([int(time.time()), 'bid', bid[0], bid[1]])
    except KeyError:
        pass


async def main():
    logger = logging.getLogger(__name__)
    # is public
    token = WsToken()
    client = Market(key=KuCoin.KEY, secret=KuCoin.SECRET, passphrase=KuCoin.PASSPRHASE, url='https://api.kucoin.com')

    # is private
    # client = WsToken(key='', secret='', passphrase='', is_sandbox=False, url='')
    # is sandbox
    # client = WsToken(is_sandbox=True)
    ws_client = await KucoinWsClient.create(None, token, deal_msg, private=False)

    for symbol in symbols:
        logger.info(f'starting symbol {symbol}')
        await ws_client.subscribe(f'/market/ticker:{symbol}')
        await ws_client.subscribe(f'/market/level2:{symbol}')

        book = client.get_aggregated_orderv3(symbol)
        buffer[symbol].extend([[book['time'], 'ask', x[0], x[1]] for x in book['asks']])
        buffer[symbol].extend([[book['time'], 'bid', x[0], x[1]] for x in book['bids']])

    while True:
        await asyncio.sleep(5)

        for sym, buf in buffer.items():
            buf_size = len(buf)
            if buf_size > parquet_size:
                logger.info(f'writing {sym}')
                today = datetime.today()
                dir_name = f'./data/symbol={sym}/year={today.year}/month={today.month}/day={today.day}/hour={today.hour}/minute={today.minute}/'
                if not os.path.exists(dir_name):
                    os.makedirs(dir_name)
                n_files = len(os.listdir(dir_name))
                file_path = f'{dir_name}part-{n_files}-{uuid4()}.snappy.parquet'  # choose your file path
                df = pd.DataFrame([buf.popleft() for _ in range(buf_size)], columns=['time', 'type', 'price', 'amount'])
                df.to_parquet(file_path)

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
