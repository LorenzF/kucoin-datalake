import argparse
import os
from uuid import uuid4

import pandas as pd

# Initialize parser
parser = argparse.ArgumentParser()

# Adding optional argument
parser.add_argument("-D", "--dir", help="directory to zip")

# Read arguments from command line
args = parser.parse_args()

if __name__ == "__main__":

    if os.path.exists(args.dir):
        folder = args.dir
    else:
        folder = "./"

    for dir, subdirs, files in os.walk(folder):
        parquet_files = [x for x in files if x.endswith('parquet')]

        if len(parquet_files)>1:
            print(dir)
            df = pd.read_parquet(dir)
            file_path = f'{dir}/{uuid4()}.snappy.parquet'  # choose your file path
            df.sort_index(ascending=True).drop_duplicates().to_parquet(file_path)
            [os.remove(f"{dir}/{file}") for file in files]